import datetime
import sys
import logging
import config
from os import symlink
import pafy
import util
from clip import Clip

class Downloader(util.Serializable):
    def __init__(self, download_id, source):
        util.Serializable.__init__(self, config.options.get("cache_path") + "/" + download_id + ".downloader")
        self.id = download_id
        self.clip = Clip(self.id, config.options.get("cache_path") + "/" + self.id, [source])
        
    def download(self):
        pass

class DirectoryDownloader(Downloader):
    def __init__(self, download_id, source, path_to_file):
        Downloader.__init__(self, download_id, source)
        self.path_to_file = path_to_file
    
    def download(self):
        try:
            symlink(self.path_to_file, self.clip.path)
        except OSError:
            logging.error("Error creating symlink to " + self.path_to_file + " in " + self.clip.path)
            self.clip.id = ''
            
        self.clip.date = datetime.datetime.now()

class YouTubeDownloader(Downloader):
    
    def __init__(self, video_id, source):
        Downloader.__init__(self, video_id, source)
        
    def download(self):
        url = "https://www.youtube.com/watch?v=%s" % self.id
        
        try:
            video = pafy.new(url)
            best = video.getbest()
            path = self.clip.path + "." + best.extension
            self.clip.path = best.download(filepath = path, quiet = True)
        except ValueError:
            logging.error("!!! YT Downloader: ValueError while downloading " + self.id + " from " + url)
            self.clip.id = ''
        except UnicodeEncodeError:
            logging.error("!!! YT Downloader: UnicodeError while downloading " + self.id + " from " + url)
            self.clip.id = ''
        except:
            logging.error("Unexpected error: ", sys.exc_info()[0])
            self.clip.id = ''
        
        self.clip.date = datetime.datetime.now()
        
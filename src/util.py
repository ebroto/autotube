import pickle
import glob
import logging
from os import path
from os import walk
from os import unlink

def serialize_metadata(what, where):
    metadata_file = None
    try:
        metadata_file = open(where, "w")
        pickle.dump(what, metadata_file)
    except:
        logging.error("Error saving metadata file " + where)
    finally:
        if metadata_file:
            metadata_file.close()

def deserialize_metadata_dir(from_dir, mask):
    metadata = []
    
    for filename in glob.glob(path.join(from_dir, mask)):
        metadata_file = open(filename, "r")
        try:
            obj = pickle.load(metadata_file)
        except:
            logging.error("Error loading metadata file " + filename + " from disk")
        else:
            metadata.append(obj)
        finally:
            metadata_file.close()
            
    return metadata

def get_dir_size(start_path = '.'):
    total_size = 0
    for dirpath, dirnames, filenames in walk(start_path):
        for f in filenames:
            fp = path.join(dirpath, f)
            total_size += path.getsize(fp)
    return total_size

def megabytes(in_bytes):
    return int(in_bytes) / 1000000

def remove_file(in_path):
    success = False
    
    try:
        unlink(in_path)
        success = True
    except OSError:
        logging.error("Error removing file " + in_path)
        
    return success

class Serializable:
    def __init__(self, filename):
        self.filename = filename
    
    def persist(self):
        serialize_metadata(self, self.filename)
        
    def forget(self):
        remove_file(self.filename)
    
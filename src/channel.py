import logging
from os import remove
import datetime
import config
import util
import player
import instance

class Channel:

    def __init__(self, name, sources, cache_path = "."):
        self.name = name
        self.sources = sources
        self.cache_path = cache_path
        self.clips = []
        self.current_clip = -1
        self.play_previous = False
        
    def serialize(self):
        filename = self.cache_path + "/" + self.name + ".channel"        
        util.serialize_metadata(self, filename)
        
    def new_clip(self, new_clip):
        exists = False
        
        for clip in self.clips:
            if clip.path == new_clip.path:
                exists = True
                break
            
        if not exists:
            self.clips.append(new_clip)
            self.serialize()
            logging.debug("CHANNEL[" + self.name + "]: Added clip " + new_clip.path)
            
    def removed_clip(self, removed_clip):
        #TODO: check if it is being played
        clip_count = len(self.clips)
        current_id = self.clips[self.current_clip].id
        self.clips = [clip for clip in self.clips if clip.id != removed_clip.id]
        
        if len(self.clips) != clip_count:
            self.set_current_clip(current_id)
            self.serialize()
        
    def set_current_clip(self, clip_id):
        index = 0
        self.current_clip = -1
        for clip in self.clips:
            if clip.id == clip_id:
                self.current_clip = index
                break
            else:
                index += 1
                
    def next(self):
        self.stop()
    
    def previous(self):
        self.stop()
        self.play_previous = True
                
    def stop(self):
        player.video_player.stop()
                
    def update(self):
        playing = ''
        stopped = ''
        
        if len(self.clips) > 0:
            if player.video_player.is_idle():
                if self.current_clip != -1:
                    stopped = self.clips[self.current_clip].id
                
                if self.current_clip == -1:
                    self.current_clip = 0
                else:
                    if self.play_previous:
                        if self.current_clip - 1 >= 0:
                            self.current_clip -= 1
                        else:
                            self.current_clip = len(self.clips) - 1
                        self.play_previous = False
                    else:
                        if self.current_clip + 1 < len(self.clips):
                            self.current_clip += 1
                        else:
                            self.current_clip = 0

                playing = self.clips[self.current_clip].id
                self.serialize()
                player.video_player.set_clip(self.clips[self.current_clip])
                player.video_player.play()
                
        return (playing, stopped)
    
class ChannelManager:

    def __init__(self):
        self.cache_path = config.options.get("cache_path")

        # load channels from disk, update cache path and test if their clips and sources are still alive
        available_sources = instance.at.source_mgr.available_sources()
        loaded_channels = util.deserialize_metadata_dir(self.cache_path, "*.channel")
        for channel in loaded_channels:
            logging.debug("Loading channel [" + channel.name + "]...")
            channel.cache_path = self.cache_path
            
            current_clip_date = datetime.datetime.now()
            if channel.current_clip != -1 and channel.current_clip < len(channel.clips):
                current_clip_date = channel.clips[channel.current_clip].date

            # update channel sources            
            channel.sources = [source for source in channel.sources if source in available_sources]
            
            # update channel clips                
            alive_clips = []
            for clip in channel.clips:
                clip_test = instance.at.clip_mgr.get([clip.id])
                if len(clip_test) > 0:
                    alive_clips.append(clip_test[0])
                    logging.debug("    added reference to clip " + clip_test[0].id)
                else:
                    logging.debug("    removing reference to dead clip " + clip.id)
            channel.clips = alive_clips

            # update current_channel            
            index = 0
            for clip in channel.clips:
                if clip.date > current_clip_date:
                    channel.current_clip = index
                    logging.debug("    current_clip: " + str(index))
                else:
                    index += 1
            if index == len(channel.clips):
                channel.current_clip = -1
                logging.debug("    current_clip resetted")

            # serialize changes                
            channel.serialize()
            logging.debug("Channel [" + channel.name + "]: loaded " + str(len(channel.clips)) + " clips")
        self.channels = loaded_channels
        
        if len(self.channels) > 0:
            self.channel_number = 0
        else:
            self.channel_number = -1
    
    def stop(self):
        self.channels[self.channel_number].stop()
        self.channel_number = -1
        player.video_player.terminate()
        
    def add(self, name, sources):
        same_name = [channel for channel in self.channels if channel.name == name]
        if len(same_name) == 0:
            channel = Channel(name, sources, self.cache_path)
            self.channels.append(channel)
            channel.serialize()
            
            logging.debug("CHANNEL MANAGER: Added new channel " + name)
    
    def remove(self, name):
        if self.channels[self.channel_number].name == name:
            self.channel_number = -1
        
        size = len(self.channels)
        self.channels = [channel for channel in self.channels if channel.name != name]
        
        if size > len(self.channels):
            filename = self.cache_path + "/" + name + ".channel"
            remove(filename)
            logging.debug("CHANNEL MANAGER: Removed channel " + name)
    
    def current_channel(self):
        return self.channels[self.channel_number]
    
    def list(self):
        return [channel.name for channel in self.channels]
    
    def set(self, name):
        if len(self.channels) > 0:
            number = -1
            counter = 0
            for channel in self.channels:
                if channel.name == name:
                    number = counter
                    break
                else:
                    counter += 1
            if number != -1:
                self.channels[self.channel_number].stop()
                self.channel_number = number
                
    def set_number(self, number):
        if number >= 0 and number < len(self.channels):
            self.channels[self.channel_number].stop()
            self.channel_number = number
    
#     def reorder(self, new_names):
#         names_sorted = sorted(self.list())
#         new_names_sorted = sorted(new_names)
#         if new_names_sorted == names_sorted:
#             channel_number_name = self.channels[self.channel_number].name
#             channel_number = -1
#             counter = 0
#             channels = []
#             for channel_name in new_names:
#                 for channel in self.channels:
#                     if channel.name == channel_name:
#                         channels.append(channel)
#                         break
#                 if channel_name == channel_number_name:
#                     channel_number = counter
#                 counter += 1
#                         
#             self.channels = channels
#             self.channel_number = channel_number
    
    def next(self):
        old_channel = self.channel_number
        if len(self.channels) > 0:
            if self.channel_number + 1 >= len(self.channels):
                self.channel_number = 0
            else:
                self.channel_number += 1
                
        if self.channel_number != old_channel:
            self.channels[old_channel].stop()
    
    def next_clip(self):
        if self.channel_number != -1:
            self.channels[self.channel_number].next()
    
    def previous(self):
        old_channel = self.channel_number
        if len(self.channels) > 0:
            if self.channel_number - 1 < 0:
                self.channel_number = len(self.channels) - 1
            else:
                self.channel_number -= 1     
                       
        if self.channel_number != old_channel:
            self.channels[old_channel].stop()
    
    def previous_clip(self):
        if self.channel_number != -1:
            self.channels[self.channel_number].previous()
 
    def volume_up(self):
        player.video_player.volume_up()
    
    def volume_down(self):
        player.video_player.volume_down()

    def seek_left(self):
        player.video_player.seek_left()
        
    def seek_right(self):
        player.video_player.seek_right()
    
    def update_clips(self, finished, removed):
        playing = []
        stopped = []
        
        #TODO: improve data structures
        #TODO: encapsulate in method that applies predicate
        for clip in finished:
            for source in clip.sources:
                for channel in self.channels:
                    for channel_source in channel.sources:
                        if channel_source == source:
                            channel.new_clip(clip)
                            
        for clip in removed:
            for source in clip.sources:
                for channel in self.channels:
                    for channel_source in channel.sources:
                        if channel_source == source:
                            channel.removed_clip(clip)
        
        if len(self.channels) > 0:
            if self.channel_number == -1:
                self.channel_number = 0
                
            (chan_playing, chan_stopped) = self.channels[self.channel_number].update()
            if len(chan_playing) > 0:
                playing.append(chan_playing)
            if len(chan_stopped) > 0:
                stopped.append(chan_stopped)
        
        return (playing, stopped)
    
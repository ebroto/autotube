from os import path
from os import makedirs 
from os import environ
import logging

#TODO: threadsafe when runtime modifications used

class Config:
    def __init__(self):
        # create cache directory if not exists
        cache_path = options["cache_path"]
        if not path.isdir(cache_path):
            try:
                makedirs(cache_path)
            except IOError:
                logging.error("Fatal error: unable to create cache path directory in " + cache_path)
                raise
 
        self.config_path = path.join(cache_path, "options")
        self.load()
        self.save()
        
    def load(self):
        if path.isfile(self.config_path):
            config_file = open(self.config_path, "r")
            for line in config_file.readlines():
                if len(line) > 0 and line[0] != "#" and "=" in line:
                    pair = line.split("=", 2)
                    options[pair[0].strip()] = pair[1].strip()
            config_file.close()

    def save(self):
        config_file = open(self.config_path, "w")
        lines = []
        for key, value in options.iteritems():
            lines.append(key + "=" + value + "\n")
        config_file.writelines(lines)
        config_file.close()

# default values
cache_path = environ["HOME"] + "/.autotube"
options = {
    "youtube_api_key" : "AIzaSyAa9OdV-pcrI-xDWSD2yVyXvbuQlpk3AOk",
    "youtube_api_service" : "youtube",
    "youtube_api_version" : "v3",
    "cache_path" : cache_path,
    "max_cache_mb" : "10000",
    "max_downloads" : "2",
    "update_sources_every" : "600",
    "log_file" : cache_path + "/log",
    "create_uds_server" : "True",
    "uds_server_file" : cache_path + "/uds",
    "create_tcp_server" : "True",
    "tcp_server_port" : "47317",
    "create_service_discovery" : "True",
    "service_discovery_port" : "47318",
    "service_discovery_name" : "AutoTube",
    "service_discovery_announce_rate" : "5",
    "clip_expiration_days" : "2",
    "check_expiration_every" : "600"
}
    
config = Config()

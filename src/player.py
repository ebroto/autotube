import threading
import copy
import logging
import gtk
gtk.gdk.threads_init()
import sys
import vlc
    
class Player(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.flag = threading.Event()
        self.clip_lock = threading.Lock()
        self.clip = None
        self.idle = False
        
    def run(self):
        while not self.flag.is_set():
            stop_playing = self.update()

            if stop_playing:
                break
            
    def terminate(self):
        self.flag.set()

    def set_clip(self, clip):
        self.clip_lock.acquire()
        self.clip = copy.deepcopy(clip)
        self.clip_lock.release()
        
    def get_clip(self):
        self.clip_lock.acquire()
        clip = copy.deepcopy(self.clip)
        self.clip_lock.release()
        return clip

    def update(self):
        pass
        
    def play(self):
        pass
    
    def stop(self):
        pass

    def is_idle(self):
        pass
    
    def volume_up(self):
        pass
    
    def volume_down(self):
        pass

    def seek_left(self):
        pass
    
    def seek_right(self):
        pass

vlc_instance = vlc.Instance()

class VLCWidget(gtk.DrawingArea):
    def __init__(self, *p):
        gtk.DrawingArea.__init__(self)
        self.player = vlc_instance.media_player_new()
        
        def handle_embed(*args):
            if sys.platform == 'win32':
                self.player.set_hwnd(self.window.handle)
            else:
                self.player.set_xwindow(self.window.xid)
        self.connect("map", handle_embed)
        self.set_size_request(320, 200)        

class VLCPlayer(Player):
    def __init__(self):
        Player.__init__(self)
        
        self.vlc = VLCWidget()
        self.vlc.player.toggle_fullscreen()
        
#         self.window = gtk.Window()
#         self.window.add(self.vlc)
#         self.window.show_all()
#         self.window.connect("destroy", gtk.main_quit)

    def update(self):
        stop_playing = gtk.main_iteration(block=False)
        return stop_playing

    def play(self):
        if self.clip:
            logging.debug("Playing " + self.clip.path)
            self.vlc.player.set_media(vlc_instance.media_new(self.clip.path))
            self.vlc.player.play()
    
    def stop(self):
        self.vlc.player.stop()
            
    def is_idle(self):
        return self.vlc.player.get_state() != vlc.State.Playing

    def volume_up(self):
        vol = self.vlc.player.audio_get_volume()
        self.vlc.player.audio_set_volume(vol + 10)
    
    def volume_down(self):
        vol = self.vlc.player.audio_get_volume()
        self.vlc.player.audio_set_volume(vol - 10)
        
    def seek_left(self):
        position = self.vlc.player.get_position()
        self.vlc.player.set_position(position - 0.1)
    
    def seek_right(self):
        position = self.vlc.player.get_position()
        self.vlc.player.set_position(position + 0.1)
        
video_player = VLCPlayer()
video_player.start()

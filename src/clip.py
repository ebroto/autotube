import util
import threading
import Queue
import time
import random
import os
import logging
import datetime
import config

class Clip:
    def __init__(self, clip_id = "", path = "", sources = []):
        self.id = clip_id
        self.path = path
        self.sources = sources
        self.date = None
        self.size = 0
        self.playing = False
        self.seen = False

class Worker(threading.Thread):
    def __init__(self, manager, worker_id):
        threading.Thread.__init__(self)
        self.manager = manager
        self.worker_id = worker_id
        self.flag = threading.Event()
        
    def run(self):
        while not self.flag.is_set():
            downloader = self.manager._thread_next_downloader()
            
            if downloader != None:
                logging.debug("WORKER[" + str(self.worker_id) + "]: Downloading " + downloader.id + " <" + downloader.clip.path + ">")                
                downloader.download() #TODO: use progress function to check flag

                if len(downloader.clip.id) > 0:
                    downloader.forget()
                    downloader.clip.size = os.path.getsize(downloader.clip.path)
                    logging.debug("WORKER[" + str(self.worker_id) + "]: Download " + downloader.id + " finished (" + str(util.megabytes(downloader.clip.size)) + " MB)")
                    self.manager._thread_add_clip(downloader.clip)
                else:
                    logging.error("WORKER[" + str(self.worker_id) + "]: Error downloading " + downloader.id)                 
            else:
                time.sleep(0.05)
        
class ClipManager:
    def __init__(self):
        # cache information
        self.cache_path = config.options.get("cache_path")
        self.cache_size = util.get_dir_size(self.cache_path)

        # load clips
        self.clips_lock = threading.Lock()
        self.clips = []
        self.clips = util.deserialize_metadata_dir(self.cache_path, "*.clip")
        
        # remove expired clips
        self.clip_expiration_days = int(config.options.get("clip_expiration_days"))
        self.check_expiration_every = int(config.options.get("check_expiration_every"))
        self.check_expiration_time = time.time()
        self.check_expired_clips(force=True)

        # update playing attribute and inform about loaded clips
        for clip in self.clips:
            clip.playing = False
            logging.debug("CLIP MANAGER: Loaded clip " + clip.path + " from disk")

        # finished and removed lists initialization        
        self.finished = self.clips
        self.finished_lock = threading.Lock()
        
        self.removed_clips = []
        self.removed_lock = threading.Lock()

        # check for correct cache size
        self.check_cache_size()

        # downloads management initialization
        self.download_workers = []
        self.download_queue = Queue.Queue()
        self.download_lock = threading.Lock()

        worker_id = 0
        while worker_id < int(config.options.get("max_downloads")):
            worker = Worker(self, worker_id)
            self.download_workers.append(worker)
            worker.start()
            worker_id += 1            
        
    def stop(self):
        for download_worker in self.download_workers:
            download_worker.flag.set()
            download_worker.join()
            
        self.download_workers = []
        
    def update_playing(self, playing, stopped):
        self.clips_lock.acquire()
        index = 0
        for clip in self.clips:
            if clip.id in playing:
                self.clips[index].playing = True
            elif clip.id in stopped:
                self.clips[index].playing = False
                self.clips[index].seen = True
            index += 1
        self.clips_lock.release()

    def update_clips(self, downloaders):
        # add new downloads
        #TODO: find another way to randomize? At channel level?
        self.download_lock.acquire()
        queue_content = []
        while not self.download_queue.empty():
            queue_content.append(self.download_queue.get())
        queue_content.extend(downloaders)
        random.shuffle(queue_content)
        map(self.download_queue.put, queue_content)
        self.download_lock.release()
        
        # obtain finished downloads
        #TODO: decouple downloads from clip manager?
        self.finished_lock.acquire()
        finished = self.finished
        self.finished = []
        self.finished_lock.release()
        
        # obtain removed clips
        self.removed_lock.acquire()
        removed = self.removed_clips
        self.removed_clips = []
        self.removed_lock.release()

        # append expired clips to removed list
        removed.extend(self.check_expired_clips())
        
        return (finished, removed)
    
    def get(self, clip_ids = []):
        ret_clips = []
        self.clips_lock.acquire()
        for clip_id in clip_ids:
            for clip in self.clips:
                if clip.id == clip_id:
                    ret_clips.append(clip)
                    break
        self.clips_lock.release()
        return ret_clips
    
    def get_all(self, sources = []):
        clips = []
        filter_by_source = len(sources) > 0
        
        self.clips_lock.acquire()
        for clip in self.clips:
            if filter_by_source:
                for clip_source in clip.sources:
                    if clip_source in sources:
                        clips.append(clip)
                        break
            else:
                clips.append(clip)
        self.clips_lock.release()
        return clips

    def check_cache_size(self):
        #TODO: check for playing clips
        removed_clips = []
        max_cache_mb = int(config.options.get("max_cache_mb"))
        cache_mb = util.megabytes(self.cache_size)
        
        self.clips_lock.acquire()
        if cache_mb > max_cache_mb:            
            remove_at_least = cache_mb - max_cache_mb
            logging.debug("Cache limit exceeded. Current: " + str(cache_mb) + " MB Max: " + str(max_cache_mb) + " MB. Removing " + str(remove_at_least) + " MB")
            
            if len(self.clips) > 0:
                clips_by_date = sorted(self.clips, key=lambda x: x.date)
                for clip_by_date in clips_by_date:
                    if not clip_by_date.playing:
                        remove_at_least -= clip_by_date.size
                        self.cache_size -= clip_by_date.size
                        removed_clips.append(clip_by_date)
                        
                        if remove_at_least <= 0:
                            break
                    
                self.clips = [clip for clip in self.clips if clip not in removed_clips]
                
        self.clips_lock.release()

        self.removed_lock.acquire()
        self.removed_clips.extend(removed_clips)
        self.removed_lock.release()

        total_removed = 0        
        for removed_clip in removed_clips:
            total_removed += removed_clip.size
            logging.debug("CLIP MANAGER: Removing clip " + clip_by_date.path)
            
            util.remove_file(removed_clip.path)
            util.remove_file(removed_clip.path + ".clip")
            
        if total_removed > 0:
            logging.debug("Removed " + str(util.megabytes(total_removed)) + " MB from cache. Current size: " + str(util.megabytes(self.cache_size)) + " MB")

    def check_expired_clips(self, force=False):
        removed_clips = []
        
        if force or time.time() - self.check_expiration_time > self.check_expiration_every:
            logging.debug("CLIP MANAGER: Checking for expired clips")
            now = datetime.datetime.now()
            alive_clips = []
            dead_clips = []
            self.clips_lock.acquire()
            for clip in self.clips:
                diff = now - clip.date
                if diff.days <= self.clip_expiration_days:
                    alive_clips.append(clip)
                else:
                    dead_clips.append(clip)
            self.clips = alive_clips
            self.clips_lock.release()

            # remove expired clips            
            for clip in dead_clips:
                logging.debug("Removing expired clip " + clip.path)
                util.remove_file(clip.path)
                util.remove_file(self.cache_path + "/" + clip.id + ".clip")
                
            removed_clips.extend(dead_clips)
            self.check_expiration_time = time.time()
            
        return removed_clips

    def _thread_next_downloader(self):
        downloader = None
        
        while downloader == None:
            if not self.download_queue.empty():
                self.download_lock.acquire()         
                downloader = self.download_queue.get()
                self.download_lock.release()
                
                self.clips_lock.acquire()
                downloader_sources = []
                counter = 0
                for clip in self.clips:
                    if clip.id == downloader.clip.id:
                        downloader_sources.extend(downloader.clip.sources)
                        break
                    else:
                        counter += 1                
                
                if len(downloader_sources) > 0:
                    for source in self.clips[counter].sources:
                        for other_source in downloader_sources:
                            if source != other_source:
                                self.clips[counter].sources.append(source.name)
                                logging.debug("CLIP MANAGER: Extending sources for clip id " + downloader.clip.id + ": " + source.name)

                    logging.debug("CLIP MANAGER: Ignoring download of already existing clip with id " + downloader.clip.id)                    
                    downloader = None
                self.clips_lock.release() 
                
            else:
                break
        
        
        return downloader
    
    def _thread_add_clip(self, clip):   
        filename = self.cache_path + "/" + clip.id + ".clip"
        util.serialize_metadata(clip, filename)

        self.clips_lock.acquire()
        self.clips.append(clip)
        self.cache_size += clip.size
        self.clips_lock.release()

        self.finished_lock.acquire()
        self.finished.append(clip)
        self.finished_lock.release()
        
        self.check_cache_size()
        
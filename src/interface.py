import SocketServer
import socket
import pickle
import os
import threading
import time
import logging
import instance
import config

#TODO: (maybe) separate the machine that downloads content from
#TODO: (maybe) the machine that plays the content. Right now
#TODO: (maybe) we are separating the machine that controls which
#TODO: (maybe) content is played from the one that downloads and plays.
#TODO: (related) user sessions. 

#TODO: (maybe) use Twisted
#TODO: (maybe) use http instead of raw tcp. Use standard SSDP.

#TODO: authentication

# structured data exchanged between client and server during some requests
class AddChannelData:
    def __init__(self, name, sources):
        self.name = name
        self.sources = sources

# handlers for each request
def on_list_channels(data_in, data_out):
    data_out.write(pickle.dumps(instance.at.channel_mgr.list()))

def on_add_channel(data_in, data_out):
    channel_info = pickle.loads(data_in.read_all())
    instance.at.channel_mgr.add(channel_info.name, channel_info.sources)    

def on_remove_channel(data_in, data_out):
    instance.at.channel_mgr.remove(data_in.readline())

def on_next_channel(data_in, data_out):
    instance.at.channel_mgr.next()    

def on_previous_channel(data_in, data_out):
    instance.at.channel_mgr.previous()

def on_next_clip(data_in, data_out):
    instance.at.channel_mgr.next_clip()
    
def on_previous_clip(data_in, data_out):
    instance.at.channel_mgr.previous_clip()

def on_current_name(data_in, data_out):
    name = instance.at.channel_mgr.current_channel().name
    data_out.write(name)
    logging.debug("Sent response: " + name)

def on_volume_up(data_in, data_out):
    instance.at.channel_mgr.volume_up()

def on_volume_down(data_in, data_out):
    instance.at.channel_mgr.volume_down()

def on_seek_left(data_in, data_out):
    instance.at.channel_mgr.seek_left()

def on_seek_right(data_in, data_out):
    instance.at.channel_mgr.seek_right()

def on_interface_ping(data_in, data_out):
    data_out.write("pong")

def on_autotube_stop(data_in, data_out):
    instance.at.exit = True

# associate operations to commands
#TODO: add Lock if operations need to be altered during runtime
#TODO: another way to do that: managers handle the requests
operations = { 
    "autotube" : {
        "stop" :            on_autotube_stop
    },
    "channel" : {
        "list" :            on_list_channels,
        "add" :             on_add_channel,
        "remove" :          on_remove_channel,
        "next" :            on_next_channel,
        "previous" :        on_previous_channel,
        "next_clip" :       on_next_clip,
        "previous_clip" :   on_previous_clip,
        "current_name" :    on_current_name,
        "volume_up" :       on_volume_up,
        "volume_down" :     on_volume_down,
        "seek_left" :       on_seek_left,
        "seek_right" :      on_seek_right
    },
    "interface" : {
        "ping" :            on_interface_ping
    }
}

class AutoTubeStreamHandler(SocketServer.StreamRequestHandler):
    timeout = 15;
    """ Handler for stream socket servers """
    def handle(self):
        logging.debug("Session created: " + str(self.client_address))
        
        while True:
            try:
                manager = self.rfile.readline().strip()
                command = self.rfile.readline().strip()
                
                found = False
                if manager in operations:
                    manager_ops = operations.get(manager)
                    if command in manager_ops:
                        found = True
                        if (command != "ping"):
                            logging.debug("Processing request. manager: [" + manager + "] command: [" + command + "]")                            
                        manager_ops.get(command)(self.rfile, self.wfile)
                        
                if not found:
                    if (len(manager) > 0):
                        logging.error("Ignoring unknown request. manager: [" + manager + "] command: [" + command + "]")
                    break
            except socket.timeout:
                logging.debug("Session timed out: " + str(self.client_address))
                break
            
        logging.debug("Session closed: " + str(self.client_address))

class ThreadedUDSServer(SocketServer.ThreadingMixIn, SocketServer.UnixStreamServer):
    pass

class ThreadedTCPServer(SocketServer.ThreadingMixIn, SocketServer.TCPServer):
    pass

class SDServer(threading.Thread):
    def __init__(self):
        threading.Thread.__init__(self)
        self.socket = socket.socket(socket.AF_INET, socket.SOCK_DGRAM)
        self.socket.bind(("", 0))
        self.socket.setsockopt(socket.SOL_SOCKET, socket.SO_BROADCAST, 1)
        self.port = int(config.options.get("service_discovery_port"))
        self.flag = threading.Event()

        name = config.options.get("service_discovery_name").replace(",", "") + " at " + socket.gethostname()
        port = config.options.get("tcp_server_port").replace(",", "")
        self.message = "|" + ",".join([name, port]) + "|"
        
    def run(self):
        rate = int(config.options.get("service_discovery_announce_rate"))
        while not self.flag.is_set():
#  error: [Errno 101] Network is unreachable
            self.socket.sendto(self.message, ("<broadcast>", self.port))
            #logging.debug("Announced service on UDP port " + str(self.port))
            
            slept = 0
            while slept < rate and not self.flag.is_set():
                time.sleep(1)
                slept += 1
            
        self.socket.close()

class InterfaceManager:
    def __init__(self):
        self.sd_server = None
        
        if bool(config.options.get("create_uds_server")):
            uds_path = config.options.get("uds_server_file")
            try:
                os.unlink(uds_path)
            except OSError:
                if os.path.exists(uds_path):
                    logging.debug("Error removing unix domain socket file")
                    raise
            
            self.uds_server = ThreadedUDSServer(uds_path, AutoTubeStreamHandler)
            self.uds_server_thread = threading.Thread(target=self.uds_server.serve_forever)
            self.uds_server_thread.start()
            logging.debug("INTERFACE: UDS server listening on file " + uds_path)

        if bool(config.options.get("create_tcp_server")):
            host, port = "", int(config.options.get("tcp_server_port"))
            ThreadedTCPServer.allow_reuse_address = True
            self.tcp_server = ThreadedTCPServer((host, port), AutoTubeStreamHandler)
            self.tcp_server_thread = threading.Thread(target=self.tcp_server.serve_forever)
            self.tcp_server_thread.start()
            logging.debug("INTERFACE: TCP server listening on port " + str(port))
            
            if bool(config.options.get("create_service_discovery")):
                self.sd_server = SDServer()
                self.sd_server.start()
                logging.debug("INTERFACE: SD server announcing service on UDP port " + str(self.sd_server.port))
        
    def stop(self):
        if self.uds_server_thread.is_alive():
            self.uds_server.shutdown()
            self.uds_server_thread.join()
            self.uds_server.server_close()
            logging.debug("INTERFACE: UDS server stopped");
            
        if self.sd_server and self.sd_server.is_alive():
            self.sd_server.flag.set()
            self.sd_server.join()
            
        if self.tcp_server_thread.is_alive():
            self.tcp_server.shutdown()
            self.tcp_server_thread.join()        
            self.tcp_server.server_close()
            logging.debug("INTERFACE: TCP server stopped");
            
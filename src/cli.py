import config
import socket
import sys

class CLI:    
    def cycle(self):
        print "                   (u) prev. channel             "
        print "                                                 "
        print "(l) prev. clip     (P)ause          next clip (r)"
        print "                                                 "
        print "                (d) next channel                 "
        print "                                                 "
        print "                                                 "
        
        line = raw_input("input> ").strip()
        if len(line) > 0:
            option = line[0].lower()
            if option == "l":
                self.send("channel", "previous_clip")
            elif option == "r":
                self.send("channel", "next_clip")
            elif option == "u":
                self.send("channel", "previous")
            elif option == "d":
                self.send("channel", "next")
            elif option == "p":
                #TODO:
                pass
    
    def shutdown(self):
        print "Stopping CLI..."
    
    def send(self, manager, command):
        sock = socket.socket(socket.AF_UNIX, socket.SOCK_STREAM)
        server_address = config.options.get("uds_server_file")
        try:
            sock.connect(server_address)
        except socket.error, msg:
            print >> sys.stderr, "Error connecting to server at " + server_address + ": " + msg
            raise
        except:
            raise

        try:
            message = manager + "\n" + command + "\n"
            print "Sending message: " + message
            sock.sendall(message)
        except:
            raise
        finally:
            sock.close()
        
if __name__ == "__main__":
    client = CLI()
    while True:
        try:
            client.cycle()
        except:
            client.shutdown()
            break
        
import time
import logging
import random

from source import SourceManager, YouTubeUserSource, DirectorySource
from channel import ChannelManager
from clip import ClipManager
from interface import InterfaceManager
import instance
import config

class autotube:

    def __init__(self):
        instance.at = self
        random.seed()
        logging.basicConfig(
            filename = config.options.get("log_file"), 
            filemode = "w", 
            level = logging.DEBUG,
            format = '%(asctime)s %(name)-12s %(levelname)-8s %(message)s',
            datefmt = '%m-%d %H:%M:%S'
        )
        
        logging.debug("--------------------------------------------------")
        logging.debug("Starting program...")

        logging.debug("Configuration options:")
        for key, value in config.options.iteritems():
            logging.debug("    " + key + ":" + value)
        
        self.exit = False
        self.source_mgr = SourceManager()
        self.clip_mgr = ClipManager()
        self.channel_mgr = ChannelManager()
        self.interface_mgr = InterfaceManager()
        
    def run(self):
        logging.debug("Running...")
        while not self.exit:
            downloaders = self.source_mgr.update_sources()
            (finished, removed) = self.clip_mgr.update_clips(downloaders)
            (playing, stopped) = self.channel_mgr.update_clips(finished, removed)
            self.clip_mgr.update_playing(playing, stopped)
            time.sleep(0.05)

        logging.debug("Stopping program...")
        self.interface_mgr.stop()
        logging.debug("Interface manager stopped")
        self.channel_mgr.stop()
        logging.debug("Channel manager stopped")
        self.clip_mgr.stop()
        logging.debug("Clip manager stopped")        
        self.source_mgr.stop()
        logging.debug("Source manager stopped")
        
if __name__ == "__main__":
    autotube()

    instance.at.source_mgr.add(YouTubeUserSource("ReutersVideo"))
    instance.at.source_mgr.add(YouTubeUserSource("TheGuardian"))
    instance.at.source_mgr.add(YouTubeUserSource("CNN"))
    instance.at.source_mgr.add(YouTubeUserSource("Euronews"))
    instance.at.source_mgr.add(YouTubeUserSource("bbcnews"))
    instance.at.source_mgr.add(YouTubeUserSource("Bloomberg"))
    instance.at.source_mgr.add(YouTubeUserSource("EconomistMagazine"))
    instance.at.source_mgr.add(YouTubeUserSource("AFP"))
    instance.at.source_mgr.add(YouTubeUserSource("AssociatedPress"))
    instance.at.source_mgr.add(YouTubeUserSource("france24english"))
    instance.at.source_mgr.add(YouTubeUserSource("RussiaToday"))
    instance.at.source_mgr.add(YouTubeUserSource("ABCNews"))
    instance.at.source_mgr.add(YouTubeUserSource("NBCNews"))
    instance.at.source_mgr.add(YouTubeUserSource("AlJazeeraEnglish"))
    instance.at.source_mgr.add(YouTubeUserSource("vicenews"))

    instance.at.source_mgr.add(YouTubeUserSource("efe"))
    instance.at.source_mgr.add(YouTubeUserSource("europapress"))

        #     instance.at.source_mgr.add(YouTubeUserSource("TEDtalksDirector"))
        #     instance.at.source_mgr.add(YouTubeUserSource("TEDxTalks"))
        #     instance.at.source_mgr.add(YouTubeUserSource("NewsyTech"))
        #     instance.at.source_mgr.add(YouTubeUserSource("techcrunch"))
        #     instance.at.source_mgr.add(YouTubeUserSource("CNETTV"))
        #     instance.at.source_mgr.add(YouTubeUserSource("MotherboardTV"))
        #     instance.at.source_mgr.add(YouTubeUserSource("ScienceChannel"))
        #     instance.at.source_mgr.add(YouTubeUserSource("NationalGeographic"))
        #     instance.at.source_mgr.add(YouTubeUserSource("bigthink"))
    
# "TED Talks", "TEDtalksDirector")
# "TEDx Talks", "TEDxTalks")
# "Newsy Tech", "NewsyTech")
# "Tech Crunch", "techcrunch")
# "CNET TV", "CNETTV")
# "Motherboard", "MotherboardTV")
# "Science Channel", "ScienceChannel")
# "National Geographic", "NationalGeographic")
# "bigthink", "bigthink")
# "NSF", "VideosatNSF")
# "DNews Channel", "DNewsChannel")
# "Newsy Science", "NewsyScience")
    
#             galactica = DirectorySource("/media/edu/Seagate Backup Plus Drive/videos/series/battlestar_galactica")
#             galactica = DirectorySource("/home/edu/test2")
#             instance.at.source_mgr.add(galactica)
#             instance.at.channel_mgr.add("Sci-Fi series", [galactica.name])

    instance.at.channel_mgr.add(
        "News of the world", 
        ["ReutersVideo", "TheGuardian", "CNN", 
"Euronews", "bbcnews", "Bloomberg", "Economist", "AFP", "AssociatedPress", "france24english", "RussiaToday", "ABCNews", "NBCNews", "AlJazeeraEnglish", "vicenews"
    ])

    instance.at.channel_mgr.add(
        "Noticias", 
        ["efe", "europapress"])

#     instance.at.channel_mgr.add("ScienceAndTech",
#         ["TEDtalksDirector",
#         "TEDxTalks",
#         "NewsyTech",
#         "techcrunch",
#         "CNETTV",
#         "MotherboardTV",
#         "ScienceChannel",
#         "NationalGeographic",
#         "bigthink"
#         ])
        
    instance.at.run()
    logging.debug("Program exited")
    
# "Reuters", "ReutersVideo")
# "CNN", "CNN")
# "EFE", "efe")
# "Europa Press", "europapress")
# "Euronews", "Euronews")
# "BBC News", "bbcnews")
# "Al Jazeera", "AlJazeeraEnglish")
# "Russia Today", "RussiaToday")
# "France 24 English", "france24english")
# "Vice News", "vicenews")
# "Bloomberg", "Bloomberg")    
# "The Guardian", "TheGuardian")
# "Economist", "EconomistMagazine")    
# "AFP", "AFP")
# "Associated Press", "AssociatedPress")
# "Newsy", "NewsyHub")
# "ABC News", "ABCNews")
# "NBC News", "NBCNews")
# "FOX News", "FoxNewsChannel")
    
# "Newsy Tech", "NewsyTech")
# "Tech Crunch", "techcrunch")
# "DNews Channel", "DNewsChannel")
# "CNET TV", "CNETTV")
# "Science Channel", "ScienceChannel")
# "Newsy Science", "NewsyScience")
# "National Geographic", "NationalGeographic")
# "NSF", "VideosatNSF")
# "TED Talks", "TEDtalksDirector")
# "TEDx Talks", "TEDxTalks")
# "bigthink", "bigthink")
# "Motherboard", "MotherboardTV")

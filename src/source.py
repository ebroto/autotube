import util
import threading
import Queue
import time
import logging
import random
import glob
import pymediainfo
from os import remove
from os import path
from enum import Enum
from apiclient.discovery import build
from downloader import YouTubeDownloader, DirectoryDownloader
import instance
import config

class Source(util.Serializable):
    def __init__(self, name, max_last_seen = 50):
        util.Serializable.__init__(self, config.options.get("cache_path") + "/" + name + ".source")
        self.name = name
        self.cache_path = config.options.get("cache_path")
        self.last_seen = []
        self.max_last_seen = max_last_seen
        
    def update(self):        
        downloaders = []
        new_downloaders = self.get_downloaders()

        for downloader in new_downloaders:
            if downloader.id not in self.last_seen:
                downloader.persist()
                downloaders.append(downloader)
                
                self.last_seen = [downloader.id] + self.last_seen
                if len(self.last_seen) > self.max_last_seen:
                    del self.last_seen[-1]
                    
                self.persist()
                
        downloaders_len = len(downloaders)
        if downloaders_len > 0:
            logging.debug("SOURCE[" + self.name + "]: Fetched " + str(downloaders_len) + " entries" if downloaders_len > 1 else " entry")
            counter = 1            
            for downloader in downloaders:
                logging.debug("     " + str(counter) + ": " + downloader.id)
                counter += 1
        else:
            logging.debug("SOURCE[" + self.name + "]: Up to date")
                
        return downloaders
    
        def get_downloaders(self):
            pass
    
class DirectorySort(Enum):
    by_name = 0
    by_date = 1
    
class DirectorySource(Source):
    def __init__(self, dir_path, max_results = 5, sort_type = DirectorySort.by_name, max_seen_ids = 500):
        Source.__init__(self, self.to_valid_name(dir_path))
        self.dir_path = dir_path
        self.max_results = max_results
        self.sort_type = sort_type
        self.max_seen_ids = max_seen_ids
        self.seen_ids = []

        #TODO: For some reason pymediainfo fails to load media information if
        #TODO: called from a thread (?). Until we solve that problem we are
        #TODO: going to load media information in the constructor.
        self.video_list = []
        #TODO: sort by date
        for filename in glob.glob(path.join(self.dir_path, "*.*")):
            media_info = pymediainfo.MediaInfo.parse(filename)
            for track in media_info.tracks:
                if track.track_type == "Video":
                    self.video_list.append(filename)
                    break

    def get_downloaders(self):
        downloaders = []
        to_retrieve = self.max_results
        own_clips = instance.at.clip_mgr.get_all([self.name])
        for clip in own_clips:
            if clip.seen:
                if clip.id not in self.seen_ids:
                    self.seen_ids = [clip.id] + self.seen_ids
                    if len(self.seen_ids) > self.max_seen_ids:
                        del self.seen_ids[-1]
                    self.persist()
            else:
                to_retrieve -= 1
                
        if to_retrieve > 0:
            for filename in self.video_list:
                downloaders.append(DirectoryDownloader(self.to_valid_name(filename), self.name, filename))
                to_retrieve -= 1
                
                if to_retrieve == 0:
                    break
                        
        return downloaders
    
    def to_valid_name(self, filename):
        return filename.replace("/", "_").replace("-", "_")
    
class YouTubeUserSource(Source):
    def __init__(self, username, max_results = 5):
        Source.__init__(self, username)
        self.max_results = max_results

    def get_downloaders(self):
        downloaders = []
        youtube = build(
            config.options.get("youtube_api_service"), 
            config.options.get("youtube_api_version"),
            developerKey = config.options.get("youtube_api_key")
        )

        channels_response = youtube.channels().list(
          forUsername = self.name,
          part = "contentDetails"
        ).execute()

        for channel in channels_response["items"]:
            uploads_list_id = channel["contentDetails"]["relatedPlaylists"]["uploads"]
            playlistitems_list_request = youtube.playlistItems().list(
                playlistId=uploads_list_id,
                part="snippet",
                maxResults=self.max_results
            )
            
            #while playlistitems_list_request:
            playlistitems_list_response = playlistitems_list_request.execute()

            for playlist_item in playlistitems_list_response["items"]:
                #title = playlist_item["snippet"]["title"]
                video_id = playlist_item["snippet"]["resourceId"]["videoId"]
                
                downloaders.append(YouTubeDownloader(video_id, self.name))

##            playlistitems_list_request = youtube.playlistItems().list_next(
##                playlistitems_list_request, playlistitems_list_response)

        return downloaders
    
class UpdateThread(threading.Thread):    
    def __init__(self, manager):
        threading.Thread.__init__(self)
        self.manager = manager
        self.queue = Queue.Queue()
        self.flag = threading.Event()
    
    def run(self):        
        while not self.queue.empty():
            source = self.queue.get()
            logging.debug("SOURCE MANAGER THREAD: Querying source " + source.name)            
            downloaders = source.update()
            self.manager._thread_add_downloaders(downloaders)
            
            if self.flag.is_set():
                break
            else:
                time.sleep(random.randint(1, 3))
                
class SourceManager:  
    def __init__(self):        
        self.cache_path = config.options.get("cache_path")
        
        self.sources = util.deserialize_metadata_dir(self.cache_path, "*.source")
        for index in xrange(len(self.sources)):
            self.sources[index].cache_path = self.cache_path            
            logging.debug("SOURCE MANAGER: Loaded source " + self.sources[index].name + " from disk")
        
        self.downloaders_lock = threading.Lock()
        self.downloaders = util.deserialize_metadata_dir(self.cache_path, "*.downloader")
        for downloader in self.downloaders:
            logging.debug("SOURCE MANAGER: Loaded pending download " + downloader.id + " from disk")

        self.update_thread = None        
        self.update_every = int(config.options.get("update_sources_every"))
        self.time = time.time() - self.update_every # TODO: monotonic
                
    #TODO: new_source replaces old source
    def add(self, new_source):
        same_name = [source for source in self.sources if source.name == new_source.name]
        
        if len(same_name) == 0:
            self.sources.append(new_source)
            new_source.persist()
            logging.debug("SOURCE MANAGER: Added new source " + new_source.name)
        else:
            logging.debug("SOURCE MANAGER: Add: Ignoring already existing source " + new_source.name)
    
    def remove(self, name):
        size = len(self.sources)
        self.sources = [source for source in self.sources if source.name != name]
        
        if size > len(self.sources):
            filename = self.cache_path + "/" + name + ".source"
            remove(filename)
            logging.debug("CHANNEL MANAGER: Removed channel " + name)
        else:
            logging.debug("CHANNEL MANAGER: Remove: Ignoring non-existent channel " + name)

    def available_sources(self):
        return [source.name for source in self.sources]
    
    def update_sources(self):
        now = time.time()

        if now - self.time > self.update_every:        
            if len(self.sources) > 0:
                launch_thread = False 
                
                if self.update_thread == None:
                    launch_thread = True
                elif not self.update_thread.is_alive():
                    self.update_thread.join()
                    launch_thread = True
                                            
                if launch_thread:
                    self.time = now
                    self.update_thread = UpdateThread(self)
                    map(self.update_thread.queue.put, self.sources)
                    self.update_thread.start()
                                    
        self.downloaders_lock.acquire()
        downloaders = self.downloaders
        self.downloaders = []
        self.downloaders_lock.release()
        
        return downloaders
        
    def stop(self):
        if self.update_thread and self.update_thread.is_alive():
            self.update_thread.flag.set()
            self.update_thread.join()
            self.update_thread = None
                
    def _thread_add_downloaders(self, downloaders):
        self.downloaders_lock.acquire()
        self.downloaders.extend(downloaders)
        self.downloaders_lock.release()
        
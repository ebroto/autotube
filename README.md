# autotube #

Proof of concept of a video feed aggregator and player that serves personalized channels. Written in Python. Plug-in approach: write a scrapper or an API based plug-in for some site as YouTube and add it as a source that will be periodically updated. Populate your channels associating them with any number of sources.

For example, you can add a “Science” channel using periodically updated YouTube user accounts specialized in science. A local directory could also be used as a source so you can add your favorite documentaries to the mix. For remote sources, a local cache of the videos is maintained in the background. Implements a discovery service so remote control applications can find it with zero configuration.